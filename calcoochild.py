#!/usr/bin/python3
# -*- coding: utf-8 -*-

import sys
import calcoo

class CalcChild(calcoo.Calc):

    def __init__(self, op1, op2):
        """Esto es el método iniciliazador"""
        self.op1 = operand1
        self.op2 = operand2


    def mul(self):
        """ Function to add the operands"""
        return self.op1 * self.op2


    def div(self):
        """ Function to add the operands"""
        return self.op1 / self.op2


if __name__ == "__main__":
     try:
        operand1 = float(sys.argv[1])
        operand2 = float(sys.argv[3])

     except ValueError:

        sys.exit("Error: first and third arguments should be numbers")

     if sys.argv[2] == "+":
        objeto = CalcChild(operand1, operand2)
        
        print(objeto.add())

     elif sys.argv[2] == "-":
        objeto = CalcChild(operand1, operand2)
        
        print(objeto.sub())
        

     elif sys.argv[2] == "*":
        mul = CalcChild(operand1, operand2)
        
        print(mul.mul())

     elif sys.argv[2] == "/":

        if sys.argv[3] == "0":
            raise ValueError("Division by zero is not allowed")
        
        else:
            div = CalcChild(operand1, operand2)
        
            print(div.div())

        

     
